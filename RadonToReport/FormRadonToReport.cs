﻿using Microsoft.Office.Interop.Word;
using pdftron.PDF;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Convert = System.Convert;
using Word = Microsoft.Office.Interop.Word;

namespace RadonToReport
{
    public partial class FormRadonToReport : Form
    {
        public bool DEBUG => checkDebug.Checked;
        private List<double> _measurements;
        private FormMessage _formMessage;
        public char[] rawReadingChars;
        private bool entered = false;
        public string rawReading;
        private string folderName = "";

        public FormRadonToReport()
        {
            InitializeComponent();
            _formMessage = new FormMessage(this);
        }

        public FormRadonToReport(FormMessage sender)
        {
            InitializeComponent();
            _formMessage = sender;
        }

        private void FormRadonToReport_Load(object sender, EventArgs e)
        {
            // Show formMessage
            this.Hide();
            _formMessage.Show();

            

            checkDebug.Visible = DEBUG;
            btnPreprocess.Visible = DEBUG;
        }

        private void FormRadonToReport_Enter(object sender, EventArgs e)
        {
            if (!entered)
            {
                // Autofill report date, end date and start date. 
                // Report date and End date are "today". 
                // Start date will be calculated from the number of measurements.
                dateReport.Value = DateTime.Now;
                dateEnd.Value = DateTime.Now;

                tbxReportNumber.Text = dateStart.Value.ToCustomRadonReportString();

                tbxPreview.Text = rawReading;

                // Extract measurements from the raw message
                _measurements = HelperMethods.ExtractMeasurementsAsDoubles(rawReading);

                // Extract EPA average from the raw message
                double epaAverage = HelperMethods.ExtractEPAAverage(rawReading);
                tbxEPA.Text = epaAverage.ToString();

                txtNumMeasurements.Text = "" + _measurements.Count + " measurements found.";

                // Fill start date and start time
                UpdateStartDateTime();

                // PDF Lib
                pdftron.PDFNet.Initialize();

                entered = true;
            }
        }

        private void PreprocessRawReading(object sender, EventArgs e)
        {
            // Extract measurements from the raw message
            _measurements = HelperMethods.ExtractMeasurementsAsDoubles(rawReading);
            
            // Extract EPA average from the raw message
            double epaAverage = HelperMethods.ExtractEPAAverage(rawReading);
            tbxEPA.Text = epaAverage.ToString();
        }

        private void btnGenerateWord_Click(object sender, EventArgs e)
        {
            // Generate Microsoft Word radon report using Template
            double durationReadings = _measurements.Count;
            double durationCalendar = HelperMethods.ComputeTestingTime(dateStart.Value, timeStart.Value, dateEnd.Value, timeEnd.Value);
            if (durationCalendar != durationReadings)
            {
                var result = MessageBox.Show("WARNING: Number of hourly readings does not match start and end dates selected. Continue generating anyway?",
                                             "WARNING",
                                             MessageBoxButtons.OKCancel);
                if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            statusStrip1.Items.Add("Generating radon report... Please wait...");

            //-------------------------------------------------------------------------------------------------------
            // Show dialog for user to choose location
            
            // Paths for template 
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string templatePath = System.IO.Path.Combine(desktop, "template3.docx");
            // Path for new radon report
            //saveFileDialog1.InitialDirectory = desktop;
            //saveFileDialog1.AddExtension = true;
            //saveFileDialog1.DefaultExt = ".docx";
            //saveFileDialog1.FileName = "Radon Report";
            //saveFileDialog1.Title = "Save Radon Report:";
            //saveFileDialog1.Filter = "All files(*.*)|(*.*)";
            //DialogResult dr = saveFileDialog1.ShowDialog();
            //if (dr != DialogResult.OK) return;
            //string newReportPath = saveFileDialog1.FileName;
            string newReportPath = folderName + "\\Radon Report.docx";

            //-------------------------------------------------------------------------------------------------------
            // Open new Word application
            object missing = Type.Missing;
            Word.Application app = new Word.Application();
            Word.Document template = app.Documents.Open(templatePath, ref missing, ReadOnly: false);
            try
            {
                template.SaveAs(newReportPath);
                template.Close();

                Word.Document newDoc = app.Documents.Open(newReportPath, ref missing, ReadOnly: false);

                // Radon test is failed if EPA Avg > 4
                bool failed = Convert.ToDouble(tbxEPA.Text.ToString()) > 4.0;

                newDoc.Bookmarks["TotalTestingTime"].Range.Text = _measurements.Count.ToString();
                newDoc.Bookmarks["StartTime"].Range.Text = timeStart.Value.ToShortTimeString();
                newDoc.Bookmarks["EndTime"].Range.Text = timeEnd.Value.ToShortTimeString();
                newDoc.Bookmarks["StartDate"].Range.Text = dateStart.Value.ToCustomDateString();
                newDoc.Bookmarks["EndDate"].Range.Text = dateEnd.Value.ToCustomDateString();
                newDoc.Bookmarks["ReportDate"].Range.Text = dateReport.Value.ToCustomDateString();
                newDoc.Bookmarks["ReportNumber"].Range.Text = tbxReportNumber.Text;
                newDoc.Bookmarks["SerialNumber"].Range.Text = tbxSerialNumber.Text;
                newDoc.Bookmarks["Location"].Range.Text = tbxLocation.Text;
                newDoc.Bookmarks["Name"].Range.Text = tbxName.Text;
                newDoc.Bookmarks["Address1"].Range.Text = tbxAddr1.Text;
                newDoc.Bookmarks["Address2"].Range.Text = tbxAddr2.Text;
                newDoc.Bookmarks["AvgEPA"].Range.Text = String.Format("{0:0.0}", Convert.ToDouble(tbxEPA.Text));
                newDoc.Bookmarks["ClassificationEPA"].Range.Text = 
                    (!failed) 
                    ? "Good" 
                    : "Failed\nEvaluation / Correction by a qualified radon mitigation contractor is recommended.";

                Word.Paragraphs pars = newDoc.Paragraphs;
                Paragraph measurementsParagraph = null;
                Paragraph hoursParagraph = null;
                foreach (Paragraph par in pars)
                {
                    if (par.Range.Text.Contains("<measurements>")) {
                        Console.WriteLine("found measurements:");
                        measurementsParagraph = par;
                    } 
                    else if (par.Range.Text.Contains("Total Testing Time"))
                    {
                        Console.WriteLine("found total hours");
                        hoursParagraph = par;
                    }
                }

                // Edit measurements
                measurementsParagraph.Range.Text = "";
                HelperMethods.FormatReading(measurementsParagraph, _measurements);

                // Edit total testing time
                WdColorIndex color = failed ? WdColorIndex.wdRed : WdColorIndex.wdGreen;
                hoursParagraph.Range.Font.ColorIndex = color;
                hoursParagraph.Next().Range.Font.ColorIndex = color;
                if (failed) hoursParagraph.Next().Next().Range.Font.ColorIndex = color;

                newDoc.Save();
                newDoc.Close();
                HelperMethods.NAR(template);
                HelperMethods.NAR(newDoc);
                app.Quit();
                HelperMethods.NAR(app);

                statusStrip1.Items.Clear();
                statusStrip1.Items.Add("DONE: Your Radon Report has been generated!");
                
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK);
            }

            PDFDoc pdfdoc = new PDFDoc();
            pdftron.PDF.Convert.OfficeToPDF(pdfdoc, newReportPath, null);
            pdfdoc.Save(folderName + "\\Radon Report.pdf", pdftron.SDF.SDFDoc.SaveOptions.e_linearized);
            pdftron.PDFNet.Terminate();
        }

        private void UpdateRadonReportNumber(object sender, EventArgs e)
        {
            tbxReportNumber.Text = dateStart.Value.ToCustomRadonReportString();
        }

        private void FormRadonToReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void FormRadonToReport_Click(object sender, EventArgs e)
        {
            statusStrip1.Items.Clear();
            ActiveControl = label9;
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        //-------------------------------------------------------------------------------------------------------------
        // (Red --> Black) when user enters valid input into text boxes
        //-------------------------------------------------------------------------------------------------------------
        private void tbxName_Leave(object sender, EventArgs e)
        {
            labelName.ForeColor = (tbxName.TextLength > 3) ? System.Drawing.Color.Black : System.Drawing.Color.Red;
        }

        private void tbxAddr1_Leave(object sender, EventArgs e)
        {
            labelAddr1.ForeColor = (tbxAddr1.TextLength > 3) ? System.Drawing.Color.Black : System.Drawing.Color.Red;
        }

        private void tbxAddr2_Leave(object sender, EventArgs e)
        {
            labelAddr2.ForeColor = (tbxAddr2.TextLength > 3) ? System.Drawing.Color.Black : System.Drawing.Color.Red;
        }

        private void tbxLocation_Leave(object sender, EventArgs e)
        {
            labelTestLocation.ForeColor = (tbxLocation.TextLength > 0) ? System.Drawing.Color.Black : System.Drawing.Color.Red;
        }

        private void timeEnd_Leave(object sender, EventArgs e)
        {
            labelEndTime.ForeColor = System.Drawing.Color.Black;
        }
        //-------------------------------------------------------------------------------------------------------------
        // Update startDateTime and Report Number boxes based on End Time
        //-------------------------------------------------------------------------------------------------------------
        private void UpdateStartDateTime()
        {
            UpdateStartDateTime(null, null);
        }

        private void UpdateStartDateTime(object sender, EventArgs e)
        {
            // Subtract number of measurements = number of hours to get start time
            timeStart.Value = timeEnd.Value.AddHours(-_measurements.Count);
            dateStart.Value = timeStart.Value;

            // Remove red color
            if (sender != null)
            {
                timeEnd_Leave(null, null);
            }
        }

        private void timeStart_ValueChanged(object sender, EventArgs e)
        {
            // Change Report Number to 1 if it is a Morning job, and 2 if it is an afternoon job
            string reportNum = tbxReportNumber.Text;
            tbxReportNumber.Text = reportNum.Remove(reportNum.Length - 1, 1) + (timeStart.Value.Hour < 14 ? "1" : "2"); 
        }

        private void btnChooseFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath;
                Console.WriteLine(folderName);
                
                if (HelperMethods.FolderNameFormatOk(folderName))
                {
                    //string[] clientInfo = HelperMethods.FolderNameExtractClientInfo(folderName);
                    string[] clientInfo = HelperMethods.ExtractClientInfoFromSummary(folderName);
                    tbxName.Text = clientInfo[0];
                    tbxAddr1.Text = clientInfo[1];
                    tbxAddr2.Text = clientInfo[2];
                }
            }


        }

        private void tbxName_TextChanged(object sender, EventArgs e)
        {
            tbxName_Leave(sender, e);
        }

        private void tbxAddr1_TextChanged(object sender, EventArgs e)
        {
            tbxAddr1_Leave(sender, e);
        }

        private void tbxAddr2_TextChanged(object sender, EventArgs e)
        {
            tbxAddr2_Leave(sender, e);
        }
    }
}
