﻿using Microsoft.Office.Interop.Word;
using pdftron;
using pdftron.PDF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Convert = System.Convert;

namespace RadonToReport
{
    static class HelperMethods
    {
        public static void FormatReading(Paragraph par, List<double> raw)
        {
            //var r = new Regex(@"(\w+)Time Interval (\d+) Hr\r\r  T  {measurements} TP{tp}\r\rOverall Avg.= {overall_avg}\rEPA Protocol Avg.= {epa_avg}{}");
            int day = 1;
            Paragraph curr;
            // Each day needs to be on a new run and needs to be made bold.
            // Each set of 24 measurements then needs to be not bold, on another run.
            string buffer = "";

            bool extraNewLine = true; // between Day # and measurements

            for (int i = 0; i < raw.Count; i++)
            {
                if (i % 24 == 0)
                {
                    if (i > 0)
                    {
                        curr = HelperMethods.InsertAndReturnParBefore(par);
                        curr.Range.Underline = WdUnderline.wdUnderlineNone;
                        curr.Range.Text = extraNewLine ? "\n" + buffer : buffer;
                    }
                    curr = HelperMethods.InsertAndReturnParBefore(par);
                    curr.Range.Underline = WdUnderline.wdUnderlineSingle;
                    curr.Range.Text = $"\nDay {day++}\n";
                    buffer = "";
                }
                buffer += string.Format("{0:0.0}", raw[i]);
                if ((i + 1) % 3 == 0) {
                    buffer += "\n";
                }
                else
                {
                    buffer += "     ";
                }

                if (i == raw.Count - 1)
                {
                    curr = HelperMethods.InsertAndReturnParBefore(par);
                    curr.Range.Text = extraNewLine ? "\n" + buffer : buffer;
                }
            }

            
        }

        public static Paragraph InsertAndReturnParBefore(Paragraph p)
        {
            p.Range.InsertParagraphBefore();
            return p.Previous();
        }

        public static Paragraph InsertAndReturnParAfter(Paragraph p)
        {
            p.Range.InsertParagraphAfter();
            return p.Next();
        }

        public static List<double> ExtractMeasurementsAsDoubles(string raw)
        {
            // Remove NewLine characters from the raw reading
            string withoutNewLines = raw.Replace("\r", "").Replace("\n", "");

            //---------------------------------------------------------------------------------------------------------------
            // Extract chunk containing the measurements

            // Starts with Time Interval 1 Hr\n\n     
            string[] startDelim = { "Hr" };
            string measurementsButWithTail = withoutNewLines.Split(startDelim, StringSplitOptions.RemoveEmptyEntries)[1];

            // Ends with "Overall Avg.= d.d
            string[] endDelim = { "Overall" };
            string measurementsStringClean = measurementsButWithTail.Split(endDelim, StringSplitOptions.RemoveEmptyEntries)[0];

            //---------------------------------------------------------------------------------------------------------------
            // Extract individial measurements as doubles

            // Measurements can be marked with T and/or P. T stands for tampered. I don't know what P stands for at this time.
            // Standard procedure is to simply ignore these markings while keeping the measurements themselves.

            var rx = new Regex(@"(\d+\.\d)");
            MatchCollection matches = rx.Matches(measurementsStringClean);
            List<double> measurements = new List<double>();
            foreach (Match match in matches)
            {
                if (match.Success) measurements.Add(Convert.ToDouble(match.Groups[1].Value));
            }

            return measurements;
        }

        public static double ExtractEPAAverage(string raw)
        {
            // Match the EPA Protocol Average with or without a whitespace in front of the number
            var rx = new Regex(@"EPA Protocol Avg.= ?(\d+\.\d)");
            MatchCollection matches = rx.Matches(raw);
            foreach (Match match in matches)
            {
                if (match.Success) return Convert.ToDouble(match.Groups[1].Value);
            }
            return Convert.ToDouble(rx.Match(raw).Groups[1].Value);
        } 

        public static void NAR(object o)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(o);
            }
            catch { }
            finally
            {
                o = null;
            }
        }

        public static double ComputeTestingTime(DateTime startDate, DateTime startTime, DateTime endDate, DateTime endTime)
        {
            DateTime start = new DateTime(startDate.Date.Year,
                                          startDate.Date.Month,
                                          startDate.Date.Day,
                                          startTime.TimeOfDay.Hours,
                                          startTime.TimeOfDay.Minutes,
                                          startTime.TimeOfDay.Seconds);

            DateTime end = new DateTime(endDate.Date.Year,
                                        endDate.Date.Month,
                                        endDate.Date.Day,
                                        endTime.TimeOfDay.Hours,
                                        endTime.TimeOfDay.Minutes,
                                        endTime.TimeOfDay.Seconds);

            double durationCalendar = Math.Floor(end.Subtract(start).TotalHours);

            return durationCalendar;
        }

        public static bool FolderNameFormatOk(string folderName)
        {
            // Expects the chosen folder path to contain the string Reports somewhere
            return folderName.Contains("Reports");
        }

        public static string[] ExtractClientInfoFromSummary(string folderName)
        {
            string[] clientInfo = new string[3];
            string pdfPath;

            clientInfo[0] = "";
            clientInfo[1] = "";
            clientInfo[2] = "";

            if (File.Exists(folderName + "\\Summary.pdf")) { pdfPath = folderName + "\\Summary.pdf"; }
            else if (File.Exists(folderName + "\\Summary Report.pdf")) { pdfPath = folderName + "\\Summary Report.pdf"; }
            else { return clientInfo; }
            
            try
            {
                PDFNet.Initialize();
                PDFDoc doc = new PDFDoc(pdfPath);
                doc.InitSecurityHandler();
                pdftron.PDF.Page page = doc.GetPage(1);
                TextExtractor txt = new TextExtractor();
                txt.Begin(page);
                String text = txt.GetAsXML(TextExtractor.XMLOutputFlags.e_words_as_elements | TextExtractor.XMLOutputFlags.e_output_bbox | TextExtractor.XMLOutputFlags.e_output_style_info);
                Console.WriteLine("\n\n- GetAsXML --------------------------\n{0}", text);
                Console.WriteLine("-----------------------------------------------------------");

                TextExtractor.Line line = txt.GetFirstLine();
                while (line.IsValid() && !line.GetFirstWord().GetString().Equals("*"))
                {
                    line = line.GetNextLine();
                }
                line = line.GetNextLine();
                string clientName = readEntireLine(line);
                line = line.GetNextLine();
                string clientAddr1 = readEntireLine(line);
                line = line.GetNextLine();
                string clientAddr2 = readEntireLine(line).Replace(" NC", ", NC");

                clientInfo[0] = clientName;
                clientInfo[1] = clientAddr1;
                clientInfo[2] = clientAddr2;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return clientInfo;
        }

        public static string readEntireLine(TextExtractor.Line line)
        {
            string clientName = "";
            for (TextExtractor.Word word = line.GetFirstWord(); word.IsValid(); word = word.GetNextWord())
            {
                clientName = clientName + word.GetString() + " ";
                Console.WriteLine(clientName);
            }
            clientName.TrimEnd();
            return clientName;
        }
    }
}
