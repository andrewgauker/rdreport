﻿using System;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace RadonToReport
{
    public partial class FormMessage : Form
    {
        private FormRadonToReport _formRadonToReport;
        private bool _receivingData = false;
        private int receivedCount = 0;
        private int tickedReceivedCount = -1;

        public FormMessage(FormRadonToReport sender)
        {
            InitializeComponent();

            _formRadonToReport = sender;
        }

        public FormMessage()
        {
            InitializeComponent();
            _formRadonToReport = new FormRadonToReport(this);
        }

        private void FormMessage_Load(object sender, EventArgs e)
        {
            label1.Text = "Searching for Radon Monitor...";
            refreshComPortList(null, EventArgs.Empty);
            bool RadonMonitorFound = findAndSelectRadonMonitor();
            label1.Text = RadonMonitorFound ? "Radon Monitor found and selected! Press begin to start." 
                : "Radon Monitor not found. Make sure it is connected and try again, or select it manually.";
            btnBegin.Enabled = RadonMonitorFound;

            btnProceed.Visible = _formRadonToReport.DEBUG;
            button1.Visible = _formRadonToReport.DEBUG;

            _formRadonToReport.Hide();
        }

        private bool findAndSelectRadonMonitor()
        {
            // Search the serial port combo box for the Radon Monitor
            foreach (object device in cbxSerialPorts.Items)
            {
                if (true)
                {
                    ComPort curr = (ComPort)device;
                    if (curr.longName.Contains("Prolific"))
                    {
                        cbxSerialPorts.SelectedItem = curr;
                        return true;
                    }
                }
            }

            return false;
        }

        private void refreshComPortList(object sender, EventArgs e)
        {
            cbxSerialPorts.Items.Clear();

            // --------------------------------------------------------------------------------------------
            // Add Serial Ports to Drop Down

            ManagementClass processClass = new ManagementClass("Win32_PnPEntity");
            ManagementObjectCollection Ports = processClass.GetInstances();
            foreach (ManagementObject property in Ports)
            {
                var portNames = SerialPort.GetPortNames();
                var name = property.GetPropertyValue("Name");
                if (name != null && name.ToString().Contains("COM"))
                {
                    string longname = property.GetPropertyValue("Caption") as string ?? string.Empty;
                    var rx = new Regex(@"((COM\d+))");
                    MatchCollection matches = rx.Matches(longname);
                    if (matches.Count > 0)
                    {
                        Match match = matches[0];
                        string shortname = match.Groups[1].Value as string;
                        cbxSerialPorts.Items.Add(new ComPort(shortname, longname));
                    }
                }
            }
        }


        private void btnBegin_Click(object sender, EventArgs e)
        {
            // Open serial port connection
            mine.PortName = ((ComPort)cbxSerialPorts.SelectedItem).name;
            mine.Open();
            if (!mine.IsOpen) throw new Exception("Serial port failed to open.");

            // Update status
            label1.Text = "Please press print on the Radon Monitor now.";
            
            // call this method when we receive data from the serial port
            mine.DataReceived += serialPort_DataReceived;

            // Make progress bar visible
            prgProgress.Visible = true;
            txtProgress.Visible = true;

            btnBegin.Enabled = false;
            cbxSerialPorts.Enabled = false;
            button2.Enabled = false;
        }

        private void serialPort_DataReceived(object sender, EventArgs e)
        {
            _receivingData = true;

            txtProgress.Text = "Reading the Radon Monitor output...";
            label1.Text = "Receiving data. Please wait...";
            receivedCount++;
            prgProgress.Value = (receivedCount < prgProgress.Maximum) ? receivedCount : prgProgress.Maximum;
        }

        private void btnProceed_Click(object sender, EventArgs e)
        {
            // Proceed if we are debugging
            if (_formRadonToReport.DEBUG)
            {
                timerDataReceived.Enabled = false;
                _formRadonToReport.Show();
                if (!_formRadonToReport.DEBUG) this.Hide();
            }
        }

        private void timerDataReceived_Tick(object sender, EventArgs e)
        {
            if (!_receivingData) return;

            if (receivedCount > tickedReceivedCount)
            {
                // We are still receiving data...
                tickedReceivedCount = receivedCount;
            } 
            else
            {
                prgProgress.Value = prgProgress.Maximum;
                // They are the same, ergo, we are done receiving data
                _receivingData = false;
                char[] myChars = new char[3000];
                mine.Read(myChars, 0, myChars.Length);
                _formRadonToReport.rawReading = new string(myChars);
                label1.Text = "All done!";
                timerDataReceived.Enabled = false;
                _formRadonToReport.Show();
                if (!_formRadonToReport.DEBUG) this.Hide();
            }
        }

        private void cbxSerialPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnBegin.Enabled = cbxSerialPorts.SelectedIndex > -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _formRadonToReport.rawReading = tbxDebugRawReading.Text;
        }

        private void FormMessage_Activated(object sender, EventArgs e)
        {

        }
    }
}
