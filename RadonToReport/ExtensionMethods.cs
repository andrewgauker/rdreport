﻿using Microsoft.Office.Interop.Publisher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadonToReport
{
    static class ExtensionMethods
    {
        public static string ToCustomDateString(this DateTime d)
        {
            // Create date string in format MM/dd/yyyy
            string month = d.Date.Month.ToString();
            if (month.Length == 1) month = "0" + month;
            string day = d.Date.Day.ToString();
            if (day.Length == 1) day = "0" + day;
            string year = d.Date.Year.ToString();
            return month + "/" + day + "/" + year;
        }

        public static string ToCustomRadonReportString(this DateTime d)
        {
            // Construct Report Number Based on the Date
            string month = d.Date.Month.ToString();
            if (month.Length == 1) month = "0" + month;
            string day = d.Date.Day.ToString();
            if (day.Length == 1) day = "0" + day;
            string year = d.Date.Year.ToString().Remove(0, 2);
            return month + day + year + (d.Hour < 14 ? "1" : "2");
        }
    }
}
