﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadonToReport
{
    class ComPort
    {
        public ComPort(string name, string longName)
        {
            this.name = name;
            this.longName = longName;
        }
        public string name { get; set; }
        public string longName { get; set; }

        public override string ToString()
        {
            return longName;
        }
    }
}
