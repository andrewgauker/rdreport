﻿namespace RadonToReport
{
    partial class FormRadonToReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dateStart = new System.Windows.Forms.DateTimePicker();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.tbxReportNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxLocation = new System.Windows.Forms.TextBox();
            this.labelTestLocation = new System.Windows.Forms.Label();
            this.tbxSerialNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelEndDate = new System.Windows.Forms.Label();
            this.dateEnd = new System.Windows.Forms.DateTimePicker();
            this.labelEndTime = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timeStart = new System.Windows.Forms.DateTimePicker();
            this.timeEnd = new System.Windows.Forms.DateTimePicker();
            this.dateReport = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGenerateWord = new System.Windows.Forms.Button();
            this.tbxPreview = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.tbxAddr1 = new System.Windows.Forms.TextBox();
            this.labelAddr1 = new System.Windows.Forms.Label();
            this.tbxAddr2 = new System.Windows.Forms.TextBox();
            this.labelAddr2 = new System.Windows.Forms.Label();
            this.tbxEPA = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnPreprocess = new System.Windows.Forms.Button();
            this.checkDebug = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txtNumMeasurements = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectClientFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // dateStart
            // 
            this.dateStart.CustomFormat = "MM/dd/yyyy";
            this.dateStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateStart.Location = new System.Drawing.Point(180, 422);
            this.dateStart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateStart.Name = "dateStart";
            this.dateStart.Size = new System.Drawing.Size(151, 30);
            this.dateStart.TabIndex = 7;
            this.dateStart.ValueChanged += new System.EventHandler(this.UpdateRadonReportNumber);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 385);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(85, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Report #";
            // 
            // tbxReportNumber
            // 
            this.tbxReportNumber.BackColor = System.Drawing.SystemColors.Window;
            this.tbxReportNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxReportNumber.Location = new System.Drawing.Point(180, 382);
            this.tbxReportNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxReportNumber.Name = "tbxReportNumber";
            this.tbxReportNumber.Size = new System.Drawing.Size(151, 30);
            this.tbxReportNumber.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(367, 385);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Report Date";
            // 
            // tbxLocation
            // 
            this.tbxLocation.BackColor = System.Drawing.SystemColors.Window;
            this.tbxLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxLocation.Location = new System.Drawing.Point(180, 326);
            this.tbxLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxLocation.Name = "tbxLocation";
            this.tbxLocation.Size = new System.Drawing.Size(488, 30);
            this.tbxLocation.TabIndex = 3;
            this.tbxLocation.Leave += new System.EventHandler(this.tbxLocation_Leave);
            // 
            // labelTestLocation
            // 
            this.labelTestLocation.AutoSize = true;
            this.labelTestLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTestLocation.ForeColor = System.Drawing.Color.Red;
            this.labelTestLocation.Location = new System.Drawing.Point(17, 330);
            this.labelTestLocation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTestLocation.Name = "labelTestLocation";
            this.labelTestLocation.Size = new System.Drawing.Size(130, 25);
            this.labelTestLocation.TabIndex = 6;
            this.labelTestLocation.Text = "Test Location";
            // 
            // tbxSerialNumber
            // 
            this.tbxSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSerialNumber.Location = new System.Drawing.Point(180, 518);
            this.tbxSerialNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxSerialNumber.Name = "tbxSerialNumber";
            this.tbxSerialNumber.Size = new System.Drawing.Size(151, 30);
            this.tbxSerialNumber.TabIndex = 12;
            this.tbxSerialNumber.Text = "1719041";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 522);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(78, 25);
            this.label4.TabIndex = 8;
            this.label4.Text = "Serial #";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 427);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(99, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "Start Date";
            // 
            // labelEndDate
            // 
            this.labelEndDate.AutoSize = true;
            this.labelEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndDate.ForeColor = System.Drawing.Color.Black;
            this.labelEndDate.Location = new System.Drawing.Point(17, 466);
            this.labelEndDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelEndDate.Name = "labelEndDate";
            this.labelEndDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelEndDate.Size = new System.Drawing.Size(93, 25);
            this.labelEndDate.TabIndex = 12;
            this.labelEndDate.Text = "End Date";
            // 
            // dateEnd
            // 
            this.dateEnd.CustomFormat = "MM/dd/yyyy";
            this.dateEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateEnd.Location = new System.Drawing.Point(180, 462);
            this.dateEnd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Size = new System.Drawing.Size(151, 30);
            this.dateEnd.TabIndex = 6;
            // 
            // labelEndTime
            // 
            this.labelEndTime.AutoSize = true;
            this.labelEndTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndTime.ForeColor = System.Drawing.Color.Red;
            this.labelEndTime.Location = new System.Drawing.Point(367, 466);
            this.labelEndTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelEndTime.Name = "labelEndTime";
            this.labelEndTime.Size = new System.Drawing.Size(96, 25);
            this.labelEndTime.TabIndex = 14;
            this.labelEndTime.Text = "End Time";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(367, 427);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 25);
            this.label8.TabIndex = 13;
            this.label8.Text = "Start Time";
            // 
            // timeStart
            // 
            this.timeStart.CustomFormat = "hh:mm tt";
            this.timeStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeStart.Location = new System.Drawing.Point(519, 422);
            this.timeStart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.timeStart.Name = "timeStart";
            this.timeStart.ShowUpDown = true;
            this.timeStart.Size = new System.Drawing.Size(151, 30);
            this.timeStart.TabIndex = 8;
            this.timeStart.ValueChanged += new System.EventHandler(this.timeStart_ValueChanged);
            // 
            // timeEnd
            // 
            this.timeEnd.CustomFormat = "hh:mm tt";
            this.timeEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeEnd.Location = new System.Drawing.Point(519, 462);
            this.timeEnd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.timeEnd.Name = "timeEnd";
            this.timeEnd.ShowUpDown = true;
            this.timeEnd.Size = new System.Drawing.Size(151, 30);
            this.timeEnd.TabIndex = 4;
            this.timeEnd.ValueChanged += new System.EventHandler(this.UpdateStartDateTime);
            // 
            // dateReport
            // 
            this.dateReport.CustomFormat = "MM/dd/yyyy";
            this.dateReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateReport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateReport.Location = new System.Drawing.Point(519, 382);
            this.dateReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateReport.Name = "dateReport";
            this.dateReport.Size = new System.Drawing.Size(151, 30);
            this.dateReport.TabIndex = 9;
            this.dateReport.TabStop = false;
            this.dateReport.Value = new System.DateTime(2020, 4, 30, 23, 58, 41, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 16);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label9.Size = new System.Drawing.Size(981, 42);
            this.label9.TabIndex = 18;
            this.label9.Text = "Hi Kurt! Enter the information for the Radon Report below. ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 60);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(685, 29);
            this.label10.TabIndex = 19;
            this.label10.Text = "Please enter information in red. The rest will be filled in for you.";
            // 
            // btnGenerateWord
            // 
            this.btnGenerateWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateWord.Location = new System.Drawing.Point(520, 572);
            this.btnGenerateWord.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGenerateWord.Name = "btnGenerateWord";
            this.btnGenerateWord.Size = new System.Drawing.Size(151, 44);
            this.btnGenerateWord.TabIndex = 5;
            this.btnGenerateWord.Text = "Generate";
            this.btnGenerateWord.UseVisualStyleBackColor = true;
            this.btnGenerateWord.Click += new System.EventHandler(this.btnGenerateWord_Click);
            // 
            // tbxPreview
            // 
            this.tbxPreview.AcceptsReturn = true;
            this.tbxPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPreview.Location = new System.Drawing.Point(705, 212);
            this.tbxPreview.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxPreview.Multiline = true;
            this.tbxPreview.Name = "tbxPreview";
            this.tbxPreview.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxPreview.Size = new System.Drawing.Size(476, 377);
            this.tbxPreview.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(700, 183);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(203, 25);
            this.label12.TabIndex = 23;
            this.label12.Text = "Radon Monitor Output";
            // 
            // tbxName
            // 
            this.tbxName.BackColor = System.Drawing.SystemColors.Window;
            this.tbxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxName.Location = new System.Drawing.Point(180, 210);
            this.tbxName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(489, 30);
            this.tbxName.TabIndex = 0;
            this.tbxName.TextChanged += new System.EventHandler(this.tbxName_TextChanged);
            this.tbxName.Leave += new System.EventHandler(this.tbxName_Leave);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.ForeColor = System.Drawing.Color.Red;
            this.labelName.Location = new System.Drawing.Point(16, 214);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelName.Size = new System.Drawing.Size(64, 25);
            this.labelName.TabIndex = 24;
            this.labelName.Text = "Name";
            // 
            // tbxAddr1
            // 
            this.tbxAddr1.BackColor = System.Drawing.SystemColors.Window;
            this.tbxAddr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddr1.Location = new System.Drawing.Point(180, 249);
            this.tbxAddr1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxAddr1.Name = "tbxAddr1";
            this.tbxAddr1.Size = new System.Drawing.Size(489, 30);
            this.tbxAddr1.TabIndex = 1;
            this.tbxAddr1.TextChanged += new System.EventHandler(this.tbxAddr1_TextChanged);
            this.tbxAddr1.Leave += new System.EventHandler(this.tbxAddr1_Leave);
            // 
            // labelAddr1
            // 
            this.labelAddr1.AutoSize = true;
            this.labelAddr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddr1.ForeColor = System.Drawing.Color.Red;
            this.labelAddr1.Location = new System.Drawing.Point(16, 252);
            this.labelAddr1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAddr1.Name = "labelAddr1";
            this.labelAddr1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelAddr1.Size = new System.Drawing.Size(143, 25);
            this.labelAddr1.TabIndex = 26;
            this.labelAddr1.Text = "Address Line 1";
            // 
            // tbxAddr2
            // 
            this.tbxAddr2.BackColor = System.Drawing.SystemColors.Window;
            this.tbxAddr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddr2.Location = new System.Drawing.Point(180, 288);
            this.tbxAddr2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxAddr2.Name = "tbxAddr2";
            this.tbxAddr2.Size = new System.Drawing.Size(489, 30);
            this.tbxAddr2.TabIndex = 2;
            this.tbxAddr2.TextChanged += new System.EventHandler(this.tbxAddr2_TextChanged);
            this.tbxAddr2.Leave += new System.EventHandler(this.tbxAddr2_Leave);
            // 
            // labelAddr2
            // 
            this.labelAddr2.AutoSize = true;
            this.labelAddr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddr2.ForeColor = System.Drawing.Color.Red;
            this.labelAddr2.Location = new System.Drawing.Point(16, 292);
            this.labelAddr2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAddr2.Name = "labelAddr2";
            this.labelAddr2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelAddr2.Size = new System.Drawing.Size(143, 25);
            this.labelAddr2.TabIndex = 28;
            this.labelAddr2.Text = "Address Line 2";
            // 
            // tbxEPA
            // 
            this.tbxEPA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxEPA.Location = new System.Drawing.Point(519, 518);
            this.tbxEPA.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxEPA.Name = "tbxEPA";
            this.tbxEPA.Size = new System.Drawing.Size(151, 30);
            this.tbxEPA.TabIndex = 11;
            this.tbxEPA.Text = "3.0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(367, 522);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label15.Size = new System.Drawing.Size(131, 25);
            this.label15.TabIndex = 30;
            this.label15.Text = "EPA Average";
            // 
            // btnPreprocess
            // 
            this.btnPreprocess.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreprocess.Location = new System.Drawing.Point(16, 574);
            this.btnPreprocess.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPreprocess.Name = "btnPreprocess";
            this.btnPreprocess.Size = new System.Drawing.Size(151, 44);
            this.btnPreprocess.TabIndex = 31;
            this.btnPreprocess.Text = "Preprocess";
            this.btnPreprocess.UseVisualStyleBackColor = true;
            this.btnPreprocess.Click += new System.EventHandler(this.PreprocessRawReading);
            // 
            // checkDebug
            // 
            this.checkDebug.AutoSize = true;
            this.checkDebug.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkDebug.Location = new System.Drawing.Point(175, 582);
            this.checkDebug.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkDebug.Name = "checkDebug";
            this.checkDebug.Size = new System.Drawing.Size(147, 29);
            this.checkDebug.TabIndex = 32;
            this.checkDebug.Text = "Debug Mode";
            this.checkDebug.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(0, 636);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1199, 22);
            this.statusStrip1.TabIndex = 33;
            this.statusStrip1.Text = "AAAAAAAAAAA";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // txtNumMeasurements
            // 
            this.txtNumMeasurements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumMeasurements.AutoSize = true;
            this.txtNumMeasurements.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumMeasurements.Location = new System.Drawing.Point(704, 593);
            this.txtNumMeasurements.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtNumMeasurements.Name = "txtNumMeasurements";
            this.txtNumMeasurements.Size = new System.Drawing.Size(227, 25);
            this.txtNumMeasurements.TabIndex = 34;
            this.txtNumMeasurements.Text = "?? measurements found.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(422, 29);
            this.label3.TabIndex = 35;
            this.label3.Text = "When you are finished, click Generate.";
            // 
            // btnSelectClientFolder
            // 
            this.btnSelectClientFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectClientFolder.Location = new System.Drawing.Point(21, 159);
            this.btnSelectClientFolder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSelectClientFolder.Name = "btnSelectClientFolder";
            this.btnSelectClientFolder.Size = new System.Drawing.Size(649, 44);
            this.btnSelectClientFolder.TabIndex = 36;
            this.btnSelectClientFolder.Text = "Select Client Folder";
            this.btnSelectClientFolder.UseVisualStyleBackColor = true;
            this.btnSelectClientFolder.Click += new System.EventHandler(this.btnChooseFolder_Click);
            // 
            // FormRadonToReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 658);
            this.Controls.Add(this.btnSelectClientFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNumMeasurements);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.checkDebug);
            this.Controls.Add(this.btnPreprocess);
            this.Controls.Add(this.tbxEPA);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tbxAddr2);
            this.Controls.Add(this.labelAddr2);
            this.Controls.Add(this.tbxAddr1);
            this.Controls.Add(this.labelAddr1);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxPreview);
            this.Controls.Add(this.btnGenerateWord);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dateReport);
            this.Controls.Add(this.timeEnd);
            this.Controls.Add(this.timeStart);
            this.Controls.Add(this.labelEndTime);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelEndDate);
            this.Controls.Add(this.dateEnd);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxSerialNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxLocation);
            this.Controls.Add(this.labelTestLocation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxReportNumber);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateStart);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormRadonToReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RDReport";
            this.Activated += new System.EventHandler(this.FormRadonToReport_Enter);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRadonToReport_FormClosing);
            this.Load += new System.EventHandler(this.FormRadonToReport_Load);
            this.Click += new System.EventHandler(this.FormRadonToReport_Click);
            this.Enter += new System.EventHandler(this.FormRadonToReport_Enter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateStart;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxReportNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxLocation;
        private System.Windows.Forms.Label labelTestLocation;
        private System.Windows.Forms.TextBox tbxSerialNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelEndDate;
        private System.Windows.Forms.DateTimePicker dateEnd;
        private System.Windows.Forms.Label labelEndTime;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DateTimePicker timeStart;
        private System.Windows.Forms.DateTimePicker timeEnd;
        private System.Windows.Forms.DateTimePicker dateReport;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnGenerateWord;
        private System.Windows.Forms.Label label12;
        internal System.Windows.Forms.TextBox tbxPreview;
        private System.Windows.Forms.TextBox tbxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox tbxAddr1;
        private System.Windows.Forms.Label labelAddr1;
        private System.Windows.Forms.TextBox tbxAddr2;
        private System.Windows.Forms.Label labelAddr2;
        private System.Windows.Forms.TextBox tbxEPA;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnPreprocess;
        private System.Windows.Forms.CheckBox checkDebug;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label txtNumMeasurements;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectClientFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}

