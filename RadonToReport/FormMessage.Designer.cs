﻿namespace RadonToReport
{
    partial class FormMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMessage));
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxSerialPorts = new System.Windows.Forms.ComboBox();
            this.timerDataReceived = new System.Windows.Forms.Timer(this.components);
            this.btnBegin = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.mine = new System.IO.Ports.SerialPort(this.components);
            this.btnProceed = new System.Windows.Forms.Button();
            this.tbxDebugRawReading = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.prgProgress = new System.Windows.Forms.ProgressBar();
            this.txtProgress = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(720, 101);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome. Choose the Radon Monitor, then press begin.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 187);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(354, 25);
            this.label11.TabIndex = 22;
            this.label11.Text = "Choose the Radon Monitor from the list:";
            // 
            // cbxSerialPorts
            // 
            this.cbxSerialPorts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSerialPorts.FormattingEnabled = true;
            this.cbxSerialPorts.Location = new System.Drawing.Point(16, 215);
            this.cbxSerialPorts.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxSerialPorts.Name = "cbxSerialPorts";
            this.cbxSerialPorts.Size = new System.Drawing.Size(351, 33);
            this.cbxSerialPorts.TabIndex = 1;
            this.cbxSerialPorts.SelectedIndexChanged += new System.EventHandler(this.cbxSerialPorts_SelectedIndexChanged);
            // 
            // timerDataReceived
            // 
            this.timerDataReceived.Enabled = true;
            this.timerDataReceived.Tick += new System.EventHandler(this.timerDataReceived_Tick);
            // 
            // btnBegin
            // 
            this.btnBegin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBegin.Location = new System.Drawing.Point(617, 214);
            this.btnBegin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(115, 36);
            this.btnBegin.TabIndex = 0;
            this.btnBegin.Text = "Begin";
            this.btnBegin.UseVisualStyleBackColor = true;
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(376, 214);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 36);
            this.button2.TabIndex = 2;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.refreshComPortList);
            // 
            // mine
            // 
            this.mine.BaudRate = 1200;
            // 
            // btnProceed
            // 
            this.btnProceed.Location = new System.Drawing.Point(259, 151);
            this.btnProceed.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProceed.Name = "btnProceed";
            this.btnProceed.Size = new System.Drawing.Size(100, 28);
            this.btnProceed.TabIndex = 26;
            this.btnProceed.Text = "Proceed";
            this.btnProceed.UseVisualStyleBackColor = true;
            this.btnProceed.Click += new System.EventHandler(this.btnProceed_Click);
            // 
            // tbxDebugRawReading
            // 
            this.tbxDebugRawReading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxDebugRawReading.Location = new System.Drawing.Point(633, 165);
            this.tbxDebugRawReading.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbxDebugRawReading.Multiline = true;
            this.tbxDebugRawReading.Name = "tbxDebugRawReading";
            this.tbxDebugRawReading.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxDebugRawReading.Size = new System.Drawing.Size(97, 42);
            this.tbxDebugRawReading.TabIndex = 27;
            this.tbxDebugRawReading.Text = resources.GetString("tbxDebugRawReading.Text");
            this.tbxDebugRawReading.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 151);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(229, 28);
            this.button1.TabIndex = 28;
            this.button1.Text = "Debug: Insert Raw Reading";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // prgProgress
            // 
            this.prgProgress.Location = new System.Drawing.Point(77, 116);
            this.prgProgress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.prgProgress.Maximum = 1500;
            this.prgProgress.Name = "prgProgress";
            this.prgProgress.Size = new System.Drawing.Size(596, 28);
            this.prgProgress.TabIndex = 29;
            this.prgProgress.Visible = false;
            // 
            // txtProgress
            // 
            this.txtProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProgress.Location = new System.Drawing.Point(91, 87);
            this.txtProgress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.Size = new System.Drawing.Size(583, 25);
            this.txtProgress.TabIndex = 30;
            this.txtProgress.Text = "Please press print on the Radon Monitor now.";
            this.txtProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.txtProgress.Visible = false;
            // 
            // FormMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 266);
            this.Controls.Add(this.txtProgress);
            this.Controls.Add(this.prgProgress);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnProceed);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnBegin);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbxSerialPorts);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxDebugRawReading);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormMessage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RDReport";
            this.Activated += new System.EventHandler(this.FormMessage_Activated);
            this.Load += new System.EventHandler(this.FormMessage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxSerialPorts;
        private System.Windows.Forms.Timer timerDataReceived;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.Button button2;
        private System.IO.Ports.SerialPort mine;
        private System.Windows.Forms.Button btnProceed;
        internal System.Windows.Forms.TextBox tbxDebugRawReading;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar prgProgress;
        private System.Windows.Forms.Label txtProgress;
    }
}